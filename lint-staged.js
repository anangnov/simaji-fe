module.exports = {
  '*.{js,jsx}': ['eslint --max-warnings=0'],
  '*.{js,jsx}': ['prettier --write'],
};
