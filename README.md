# SPACE GRID

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).


## Available Scripts

In the project directory, you can run:

```bash
npm start
```

```bash
npm run build
```


## Code Convention
#### [Airbnb](https://github.com/airbnb/javascript/tree/master/react)


## Features
#### [Tailwind CSS](https://tailwindcss.com/)
#### [Eslint](https://eslint.org/)
#### [Prettier](https://prettier.io/)


