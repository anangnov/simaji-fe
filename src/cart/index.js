import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Cart from './components/Cart';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <Cart />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
