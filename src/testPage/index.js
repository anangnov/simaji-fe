import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import TestPage from './components/TestPage';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <TestPage />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
