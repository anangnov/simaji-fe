import { client } from '../commons/services';

const version = 'api';

export const updateData = (body, id) => client.patch(`${version}/product/${id}`, body);

export const listData = () => client.get(`${version}/product`);

export const detailData = (id) => client.get(`${version}/product/${id}`);
