// eslint-disable-next-line no-unused-vars
import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Home() {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [, setResponseData] = useState({});
  const [errorMessage, setErrorMessage] = useState('');
  const [errorEmail, setErrorEmail] = useState('');
  const [errorPassword, setErrorPassword] = useState('');

  useEffect(() => {});

  // const token = window.sessionStorage.getItem('access_token');
  const header = {
    'Content-Type': 'application/json',
    // Authorization: `Bearer ${token}`,
  };

  // eslint-disable-next-line no-unused-vars
  const submitLogin = async () => {
    try {
      const { data: response } = await axios.post(
        `${process.env.REACT_APP_BASE_API}/login`,
        {
          // eslint-disable-next-line object-shorthand
          email: email,
          // eslint-disable-next-line object-shorthand
          password: password,
        },
        { headers: header },
      );

      if (response.error) {
        setErrorMessage(response.message);

        setTimeout(() => {
          setErrorMessage('');
        }, 3000);
      } else {
        localStorage.setItem('access_token', response.data.token);
        localStorage.setItem('user_id', response.data.user_id);
        toast.success('Success Login', {
          position: 'top-center',
        });

        setTimeout(() => {
          window.location.href = '/';
        }, 3000);
      }

      if (email === '') {
        setErrorEmail('Please enter your email address');

        setTimeout(() => {
          setErrorEmail('');
        }, 3000);
      }

      if (password === '') {
        setErrorPassword('Please enter your password');

        setTimeout(() => {
          setErrorPassword('');
        }, 3000);
      }

      setResponseData(response);
    } catch (error) {
      // console.error(error.message);
    }
  };

  return (
    <div
      style={{ backgroundColor: '#013281' }}
      className="flex items-center justify-center min-h-screen bg-gray-100"
    >
      <ToastContainer />
      <div className="w-1/4 px-8 py-8 mt-4 text-left bg-white shadow-lg rounded-md">
        <div className="flex justify-left pb-3">
          <p className="text-blue-700 font-bold text-2xl">Company Logo</p>
        </div>
        <h3 style={{ color: '#232933' }} className="text-md font-bold text-left">
          Login to your account
        </h3>
        <span className="text-xs tracking-wide text-red-600">{errorMessage}</span>
        <form>
          <div className="mt-6">
            <div>
              <label className="block text-sm" htmlFor="email">
                Your Email
              </label>
              <input
                type="email"
                placeholder="Email"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
                className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
              />
              <span className="text-xs tracking-wide text-red-600">{errorEmail}</span>
            </div>
            <div className="mt-4">
              <label className="block text-sm">Password</label>
              <input
                type="password"
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
                className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
              />
              <span className="text-xs tracking-wide text-red-600">{errorPassword}</span>
            </div>
            <div className="flex items-baseline justify-between">
              <button
                type="button"
                onClick={submitLogin}
                className="w-full px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg hover:bg-blue-900"
              >
                Log in
              </button>
            </div>
            <div className="pt-5 text-center">
              <a href="/" className="text-sm text-blue-600 underline hover:underline">
                Forgot password?
              </a>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
