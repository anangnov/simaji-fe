import React from 'react';
import PropTypes from 'prop-types';

export function Flag({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M6 1.49988C6.27614 1.49988 6.5 1.72374 6.5 1.99988V3.64752L15.7544 6.95267L15.7954 6.96731C16.7036 7.29165 17.4378 7.55383 17.9458 7.82125C18.4467 8.08489 18.917 8.45012 18.9541 9.06304C18.9911 9.67595 18.568 10.0951 18.1026 10.4171C17.6304 10.7437 16.9331 11.0923 16.0706 11.5236L16.0316 11.5431L6.5 16.3089V21.9999C6.5 22.276 6.27614 22.4999 6 22.4999C5.72386 22.4999 5.5 22.276 5.5 21.9999V15.9999V3.99988V1.99988C5.5 1.72374 5.72386 1.49988 6 1.49988ZM6.5 15.1909L15.5844 10.6487C16.495 10.1933 17.1273 9.87583 17.5337 9.59471C17.9538 9.30408 17.9587 9.16971 17.9559 9.12331C17.9531 9.07692 17.9321 8.94411 17.48 8.70614C17.0427 8.47597 16.3769 8.23685 15.4181 7.89441L6.5 4.70938V15.1909Z"
        fill="#232933"
      />
    </svg>
  );
}

Flag.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Flag.defaultProps = {
  width: '24px',
  height: '24px',
};
