import React from 'react';
import PropTypes from 'prop-types';

export function Dolar({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <g clipPath="url(#clip0_435_549)">
        <path
          d="M10 18.7495C14.8325 18.7495 18.75 14.832 18.75 9.99951C18.75 5.16702 14.8325 1.24951 10 1.24951C5.16751 1.24951 1.25 5.16702 1.25 9.99951C1.25 14.832 5.16751 18.7495 10 18.7495Z"
          stroke="#232933"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M12.5 6.24952H8.99083H8.99083C8.22834 6.18864 7.56087 6.75741 7.5 7.5199C7.49414 7.59327 7.49414 7.66699 7.5 7.74036C7.5 9.37452 12.5 10.6245 12.5 12.2587V12.2587C12.5149 13.0672 11.8715 13.7347 11.063 13.7495C11.045 13.7499 11.0271 13.7499 11.0092 13.7495H7.5"
          stroke="#626B79"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M10 6.24951V4.37451"
          stroke="#626B79"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
        <path
          d="M10 15.6245V13.7495"
          stroke="#626B79"
          strokeLinecap="round"
          strokeLinejoin="round"
        />
      </g>
      <defs>
        <clipPath id="clip0_435_549">
          <rect width="20" height="20" fill="white" transform="translate(0 -0.000488281)" />
        </clipPath>
      </defs>
    </svg>
  );
}

Dolar.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Dolar.defaultProps = {
  width: '24px',
  height: '24px',
};
