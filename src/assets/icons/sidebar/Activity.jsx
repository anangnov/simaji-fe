import React from 'react';
import PropTypes from 'prop-types';

export function Activity({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M6 11.9999H8L10 14.9999L14 8.99988L16 11.9999H18"
        stroke="#626B79"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <rect
        x="2"
        y="2.99988"
        width="20"
        height="18"
        rx="4"
        stroke="#232933"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

Activity.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Activity.defaultProps = {
  width: '24px',
  height: '24px',
};
