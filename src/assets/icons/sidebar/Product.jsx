import React from 'react';
import PropTypes from 'prop-types';

export function Product({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M3.99998 6H20V15C20 17.8284 20 19.2426 19.1213 20.1213C18.2426 21 16.8284 21 14 21H9.99998C7.17156 21 5.75734 21 4.87866 20.1213C3.99998 19.2426 3.99998 17.8284 3.99998 15V6Z"
        fill="#D7E6FE"
      />
      <path
        d="M4 6H20V15C20 17.8284 20 19.2426 19.1213 20.1213C18.2426 21 16.8284 21 14 21H10C7.17157 21 5.75736 21 4.87868 20.1213C4 19.2426 4 17.8284 4 15V6Z"
        stroke="#4C33FF"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path d="M9 10H15" stroke="#4C33FF" strokeLinecap="round" strokeLinejoin="round" />
      <rect
        x="2"
        y="3"
        width="20"
        height="3"
        rx="1.5"
        stroke="#4C33FF"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

Product.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Product.defaultProps = {
  width: '24px',
  height: '24px',
};
