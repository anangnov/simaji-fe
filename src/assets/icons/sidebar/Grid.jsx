import React from 'react';
import PropTypes from 'prop-types';

export function Grid({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M9.00002 2.99988L9.00002 20.9999"
        stroke="#626B79"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <rect
        x="3.00002"
        y="2.99988"
        width="18"
        height="18"
        rx="2"
        stroke="#232933"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

Grid.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Grid.defaultProps = {
  width: '24px',
  height: '24px',
};
