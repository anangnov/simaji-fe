import React from 'react';
import PropTypes from 'prop-types';

export function Invoice({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        d="M5 8.99982C5 6.17139 5 4.75718 5.87868 3.8785C6.75736 2.99982 8.17157 2.99982 11 2.99982H12.5147C13.741 2.99982 14.3541 2.99982 14.9054 3.22818C15.4567 3.45654 15.8903 3.89008 16.7574 4.75718L17.2426 5.24246C18.1097 6.10955 18.5433 6.54309 18.7716 7.09441C19 7.64572 19 8.25885 19 9.4851V14.9998C19 17.8282 19 19.2425 18.1213 20.1211C17.2426 20.9998 15.8284 20.9998 13 20.9998H11C8.17157 20.9998 6.75736 20.9998 5.87868 20.1211C5 19.2425 5 17.8282 5 14.9998V8.99982Z"
        stroke="#232933"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14 3.49988V3.99991C14 5.88553 14 6.82833 14.5858 7.41412C15.1716 7.99991 16.1144 7.99991 18 7.99991H18.5"
        stroke="#232933"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M14 10.9999H11.5C10.6716 10.9999 10 11.6715 10 12.4999V12.4999C10 13.3284 10.6716 13.9999 11.5 13.9999H12.5C13.3284 13.9999 14 14.6715 14 15.4999V15.4999C14 16.3284 13.3284 16.9999 12.5 16.9999H10"
        stroke="#626B79"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
      <path
        d="M12 10.9999V9.99994M12 16.9999V17.9999"
        stroke="#626B79"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  );
}

Invoice.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

Invoice.defaultProps = {
  width: '24px',
  height: '24px',
};
