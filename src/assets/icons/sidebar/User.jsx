import React from 'react';
import PropTypes from 'prop-types';

export function User({ width = '24px', height = '24px' }) {
  return (
    <svg
      className="w-full"
      width={width}
      height={height}
      viewBox="0 0 24 24"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path
        fillRule="evenodd"
        clipRule="evenodd"
        d="M8.5 6.99988C8.5 5.06688 10.067 3.49988 12 3.49988C13.933 3.49988 15.5 5.06688 15.5 6.99988C15.5 8.93287 13.933 10.4999 12 10.4999C10.067 10.4999 8.5 8.93287 8.5 6.99988ZM12 2.49988C9.51472 2.49988 7.5 4.5146 7.5 6.99988C7.5 9.48516 9.51472 11.4999 12 11.4999C14.4853 11.4999 16.5 9.48516 16.5 6.99988C16.5 4.5146 14.4853 2.49988 12 2.49988ZM5.5 18.9999C5.5 17.0669 7.067 15.4999 9 15.4999H15C16.933 15.4999 18.5 17.0669 18.5 18.9999C18.5 19.8283 17.8284 20.4999 17 20.4999H7C6.17157 20.4999 5.5 19.8283 5.5 18.9999ZM9 14.4999C6.51472 14.4999 4.5 16.5146 4.5 18.9999C4.5 20.3806 5.61929 21.4999 7 21.4999H17C18.3807 21.4999 19.5 20.3806 19.5 18.9999C19.5 16.5146 17.4853 14.4999 15 14.4999H9Z"
        fill="#232933"
      />
    </svg>
  );
}

User.propTypes = {
  width: PropTypes.string,
  height: PropTypes.string,
};

User.defaultProps = {
  width: '24px',
  height: '24px',
};
