/* eslint-disable no-unused-vars */
/* eslint-disable no-confusing-arrow */
/* eslint-disable function-paren-newline */
/* eslint-disable implicit-arrow-linebreak */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Listbox, Transition } from '@headlessui/react';
import { SIDE_MENU_ITEMS } from '../../settings/constants';
import avatar from '../../assets/avatar.png';

const menuItem = SIDE_MENU_ITEMS;

function SidebarListMenu({ onChange, values }) {
  return (
    <div className="rounded-box shadow-lg btn-ghost bg-white">
      <Listbox as="div" className="space-y-1" value={values} onChange={onChange}>
        {({ open }) => (
          <>
            <div className="flex relative">
              <Listbox.Button className="flex justify-center space-x-1 items-center text-gray-700 p-4">
                <div className="flex-grow avatar rounded-full w-9 h-9 mr-3 bg-gray-400">
                  {/* <span className="rounded-full w-11 h-11 m-3 bg-gray-400"> */}
                  {/* </span> */}
                </div>
                <div className="flex-shrink w-28 text-left text-sm">{values.name}</div>
                <div className="flex-grow">
                  <svg
                    width="24"
                    height="24"
                    viewBox="0 0 24 24"
                    fill="none"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <path
                      d="M17 9L12.5 5L8 9"
                      stroke="#61727D"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                    <path
                      d="M17 15L12.5 19L8 15"
                      stroke="#61727D"
                      strokeWidth="1.5"
                      strokeLinecap="round"
                      strokeLinejoin="round"
                    />
                  </svg>
                </div>
              </Listbox.Button>
              <Transition
                show={open}
                enter="transition duration-100 ease-out"
                enterFrom="transform scale-95 opacity-0"
                enterTo="transform scale-100 opacity-100"
                leave="transition duration-75 ease-out"
                leaveFrom="transform scale-100 opacity-100"
                leaveTo="transform scale-95 opacity-0"
                className="absolute mt-20 w-full rounded-md bg-white shadow-lg z-10"
              >
                <Listbox.Options static>
                  {menuItem.map((item) =>
                    item.id !== values.id ? (
                      <Listbox.Option
                        key={item.id}
                        value={item}
                        disabled={item.unavailable}
                        className="hover:bg-gray-600 hover:text-white px-4 py-2 text-sm"
                      >
                        {item.name}
                      </Listbox.Option>
                    ) : null,
                  )}
                </Listbox.Options>
              </Transition>
            </div>
          </>
        )}
      </Listbox>
    </div>
  );
}

SidebarListMenu.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  values: PropTypes.object.isRequired,
  onChange: PropTypes.func.isRequired,
};

export function Sidebar() {
  const [show, setShow] = useState(false);
  const [selectedMenu, setselectedMenu] = useState(menuItem[0]);
  return (
    <>
      <aside className="bg-white absolute shadow-sm border border-gray-100 lg:relative h-auto hidden lg:block">
        <div className="flex flex-wrap justify-center h-20 content-center">
          <span className="bg-blue-700 text-white font-bold flex items-center justify-center text-2xl w-10 h-10 rounded-full">
            S
          </span>
        </div>
        <div className="z-50 mt-2 mb-8 bg-white">
          <ul className="mx-6 w-8 text-center">
            {selectedMenu.childMenu.map((mnu) => (
              <li className="cursor-pointer text-sm leading-3 tracking-normal pb-4 pt-4">
                <Link to={mnu.path} className="flex items-center">
                  {mnu.icon}
                </Link>
              </li>
            ))}
          </ul>
        </div>
        <div className="flex justify-center items-center pt-8">
          <img src={avatar} alt="Avatar" />
        </div>
      </aside>
    </>
  );
}
