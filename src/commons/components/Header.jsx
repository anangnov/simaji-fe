/* eslint-disable no-unused-vars */
import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Menu, Transition } from '@headlessui/react';

// {/* 1. NavLeft */}
const logout = async () => {
  localStorage.removeItem('access_token');

  setTimeout(() => {
    window.location.href = '/';
  }, 1000);
};

function NavLeft() {
  return (
    <>
      <div className="flex justify-between items-center lg:flex flex justify-between">
        <div>
          <p className="ml-5 text-xl font-bold">Company Name</p>
        </div>
        <div>
          <Link to="/cart" className="ml-5 text-sm font-bold">Cart</Link>
          <button onClick={logout} type="button" className="ml-5 text-sm font-bold">Logout</button>
        </div>
      </div>
    </>
  );
}

function NavWrapper({ children }) {
  return <header className="p-4">{children}</header>;
}

NavWrapper.propTypes = {
  children: PropTypes.node.isRequired,
};

export function Header() {
  return (
    <NavWrapper>
      <NavLeft />
    </NavWrapper>
  );
}
