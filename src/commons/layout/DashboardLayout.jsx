/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Header, Footer, Sidebar } from '../components';

export default function DashboardLayout({ children }) {
  // const [show, setShow] = useState(false);
  const [profile, setProfile] = useState(false);
  const [menu, setMenu] = useState(false);
  const [menu1, setMenu1] = useState(false);
  const [menu2, setMenu2] = useState(false);
  const [menu3, setMenu3] = useState(false);

  return (
    <>
      <div className="w-full h-full">
        <div className="flex flex-no-wrap">
          {/* <Sidebar /> */}
          <div className="w-full flex flex-col min-h-screen">
            <Header />
            <main className="flex-grow" style={{ backgroundColor: '#F3F5F8' }}>
              {children}
            </main>
            <Footer />
          </div>
        </div>
      </div>
    </>
  );
}

DashboardLayout.propTypes = {
  children: PropTypes.node.isRequired,
};
