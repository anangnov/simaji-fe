import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Button from './components/Button';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <Button />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
