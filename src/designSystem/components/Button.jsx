import DashboardLayout from '../../commons/layout/DashboardLayout';
import '../css/custom.css';

export default function Button() {
  return (
    <DashboardLayout>
      <div className="card shadow">
        <div className="card-body">
          <h2 className="card-title">Blank Fix</h2>
          <div className="card-actions">
            <div className="grid grid-cols-2 gap-4">
              <button
                type="submit"
                className="nadi-primary  text-white  font-bold py-2 px-6 rounded"
              >
                Primary
              </button>
              <p className="text-primary">The quick brown fox...</p>
              <button
                type="submit"
                className="nadi-secondary text-white font-bold py-2 px-6 rounded"
              >
                Secondary
              </button>
              <p className="text-secondary">The quick brown fox...</p>
              <button type="submit" className="nadi-tertiary font-bold py-2 px-6 rounded">
                Tertiary
              </button>
              <p className="text-tertiary">The quick brown fox...</p>
              <button
                type="submit"
                className="nadi-warning   text-white font-bold py-2 px-6 rounded"
              >
                Warning
              </button>
              <p className="text-warning">The quick brown fox...</p>
              <button type="submit" className="nadi-error text-white font-bold py-2 px-6 rounded">
                Error
              </button>
              <p className="text-error">The quick brown fox...</p>
              <button type="submit" className="nadi-success text-white font-bold py-2 px-6 rounded">
                Success
              </button>
              <p className="text-success">The quick brown fox...</p>
              <button
                type="submit"
                className="nadi-basic-bl text-white font-bold py-2 px-6 rounded"
              >
                Basic
              </button>
              <p className="text-basic-bl">The quick brown fox...</p>
            </div>
          </div>
        </div>
      </div>
    </DashboardLayout>
  );
}
