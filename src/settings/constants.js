// import { MenuIconDefault } from '../assets/icons/MenuIconDefault';
import { Home } from '../assets/icons/sidebar/Home';
import { Activity } from '../assets/icons/sidebar/Activity';
import { Flag } from '../assets/icons/sidebar/Flag';
import { Grid } from '../assets/icons/sidebar/Grid';
import { Invoice } from '../assets/icons/sidebar/Invoice';
import { Setting } from '../assets/icons/sidebar/Setting';
import { User } from '../assets/icons/sidebar/User';
import { Dolar } from '../assets/icons/sidebar/Dolar';
import { Product } from '../assets/icons/sidebar/Product';

// **************** ROUTE CONSTANT START **************************
const globalMenus = [
  { id: 1, name: '', path: '/', exact: true, icon: <Flag /> },
  { id: 2, name: '', path: '/', exact: false, icon: <Home /> },
  { id: 3, name: '', path: '/', exact: false, icon: <User /> },
  { id: 4, name: '', path: '/', exact: false, icon: <Dolar /> },
  { id: 5, name: '', path: '/', exact: false, icon: <Invoice /> },
  { id: 6, name: '', path: '/', exact: false, icon: <Activity /> },
  { id: 7, name: '', path: '/', exact: false, icon: <Product /> },
  { id: 8, name: '', path: '/', exact: false, icon: <Grid /> },
  { id: 9, name: '', path: '/', exact: false, icon: <Setting /> },
];

export const SIDE_MENU_ITEMS = [
  { id: 1, name: 'Global', childMenu: globalMenus, icon: '', unavailable: false },
];
// **************** ROUTE CONSTANT END **************************

export const CURRENCY = 'IDR';
