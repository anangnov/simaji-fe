/* eslint-disable no-unused-expressions */
/* eslint-disable no-confusing-arrow */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable implicit-arrow-linebreak */
import React, { useEffect, lazy, Suspense } from 'react';
import { HashRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import Home from '../home';
import Cart from '../cart';
import anotherPage from '../anotherPage';
import TestPage from '../testPage';
import designSystem from '../designSystem';
import Login from '../auth/components/Login';

const accessToken = localStorage.getItem('access_token');

function PrivateRoute({ children, ...rest }) {
  useEffect(() => {
    console.log('route');
  }, []);
  return (
    <Route
      {...rest}
      render={({ location }) =>
        accessToken ? (
          children
        ) : (
          <Redirect
            to={{
              pathname: '/auth',
              state: { from: location },
            }}
          />
        )}
    />
  );
}

export default function index() {
  return (
    <Router>
      <Switch>
        <PrivateRoute exact path="/">
          <Home />
        </PrivateRoute>
        <PrivateRoute exact path="/cart">
          <Cart />
        </PrivateRoute>
        <Route path="/anotherPage" component={anotherPage} />
        <Route path="/testPage" component={TestPage} />
        <Route path="/designSystem" component={designSystem} />
        <Route path="/auth" component={Login} />
      </Switch>
    </Router>
  );
}
