import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import HomeGlobal from './components/Home';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <HomeGlobal />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
