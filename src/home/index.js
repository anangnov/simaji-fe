import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Home from './components/Home';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <Home />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
