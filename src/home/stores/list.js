import { createStore, createHook } from 'react-sweet-state';
import { list } from '../api';

const Store = createStore({
  initialState: {
    isLoading: false,
    limit: 10,
    skip: 0,
    search: '',
    sort: 'desc',
    data: [],
    total: 0,
    error: null,
    form: {},
  },
  actions: {
    updateStore:
      (newState) => async ({ setState }) => {
        setState(newState);
      },
    loadData:
      () => async ({ setState, getState }) => {
        setState({ isLoading: true });

        try {
          const res = await list(getState());
          if (res.data && res.data.code === 200) {
            setState({ data: res.data.data, total: res.data.total, isLoading: false });
          }
        } catch (err) {
          setState({
            error: err && err.message ? err.response.message : 'Data not found',
          });
        }
      },
  },
  name: 'list',
});

export const useListStore = createHook(Store);
