/* eslint-disable no-unused-vars */
/* eslint-disable object-shorthand */
import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { ToastContainer, toast } from 'react-toastify';
import axios from 'axios';
import Modal from 'react-modal';
import Swal from 'sweetalert2';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faXmark,
  faPlus,
  faCheckCircle,
  faCirclePlus,
  faEllipsis,
} from '@fortawesome/free-solid-svg-icons';
import ReactLoading from 'react-loading';
import DashboardLayout from '../../commons/layout/DashboardLayout';
import rocket from '../../assets/rocket.png';
// import plusOval from '../../assets/plus_oval.png';
// import simpleLayout from '../../assets/simple_layout.png';
import '../css/custom.css';

const token = window.localStorage.getItem('access_token');
const header = {
  'Content-Type': 'application/json',
  Authorization: `Bearer ${token}`,
};

export default function Home() {
  const [modalIsOpen, setIsOpen] = useState(false);
  const [product, setProduct] = useState();
  const [loading, isLoading] = useState(true);
  const [productName, setProductName] = useState('');
  const [description, setDescription] = useState('');
  const [price, setPrice] = useState('');
  const [qty, setQty] = useState('');
  const [cssButton, setCssButton] = useState('opacity-50 cursor-not-allowed');
  const [success, setSuccess] = useState(false);

  const fetchProduct = async () => {
    const data = await axios.get(`${process.env.REACT_APP_BASE_API}/products`, { headers: header });
    setProduct(data.data);
    setTimeout(() => {
      isLoading(false);
    }, 1400);
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  // eslint-disable-next-line no-unused-vars
  const openModal = () => {
    setIsOpen(true);
  };

  const closeModal = () => {
    setIsOpen(false);
  };

  // eslint-disable-next-line no-unused-vars
  const submitProduct = async () => {
    const res = await axios.post(
      `${process.env.REACT_APP_BASE_API}/product`,
      {
        name: productName,
        qty: qty,
        description: description,
        price: price,
      },
      { headers: header },
    );

    if (!res.error) {
      setSuccess(true);

      setTimeout(() => {
        window.location.reload();
      }, 1000);
    }
  };

  const addToCart = async (productId) => {
    const userId = localStorage.getItem('user_id');
    const res = await axios.post(
      `${process.env.REACT_APP_BASE_API}/cart/add-to-cart`,
      {
        user_id: userId,
        product_id: productId,
        qty: 1
      },
      { headers: header },
    );

    if (!res.error) {
      setSuccess(true);

      setTimeout(() => {
        Swal.fire(
          'Add to cart',
          '',
          'success'
        );
      }, 1000);
    }
  };

  return (
    <DashboardLayout>
      <ToastContainer />
      <Modal
        className="w-1/3 m-auto mt-12"
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        contentLabel="Imgae Modal Medical History"
      >
        <div>
          <div className="bg-white shadow-md rounded-md">
            <div
              className="px-4 py-3 flex justify-between items-center"
              style={{ backgroundColor: '#F3F5F8' }}
            >
              <p className="text-md font-medium">Add New Product</p>
              <FontAwesomeIcon
                onClick={closeModal}
                className="text-lg cursor-pointer"
                style={{ color: '#626B79' }}
                icon={faXmark}
              />
            </div>
            <div className="px-4 py-4">
              <div className="mb-5">
                <label className="block text-sm" htmlFor="productName">
                  Product Name
                </label>
                <input
                  type="email"
                  onChange={(e) => setProductName(e.target.value)}
                  placeholder="Product Name"
                  className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="mb-5">
                <label className="block text-sm" htmlFor="productName">
                  Description
                </label>
                <input
                  type="email"
                  onChange={(e) => setDescription(e.target.value)}
                  placeholder="Description"
                  className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="mb-5">
                <label className="block text-sm" htmlFor="productName">
                  Qty
                </label>
                <input
                  type="email"
                  onChange={(e) => setPrice(e.target.value)}
                  placeholder="Qty"
                  className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="mb-5">
                <label className="block text-sm" htmlFor="productName">
                  Price
                </label>
                <input
                  type="email"
                  onChange={(e) => setQty(e.target.value)}
                  placeholder="Price"
                  className="w-full text-sm px-4 py-2 mt-2 border rounded-md focus:outline-none focus:ring-1 focus:ring-blue-600"
                />
              </div>
              <div className="flex items-baseline justify-end mb-1">
                <button
                  type="button"
                  onClick={submitProduct}
                  className="inline-flex px-6 py-2 mt-4 text-white text-sm bg-blue-600 rounded-lg hover:bg-blue-900"
                >
                  {!success ? (
                    'Submit Product'
                  ) : (
                    <>
                      <svg
                        className="w-5 h-5 mr-3 -ml-1 text-green-500 animate-spin"
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                      >
                        <circle
                          className="opacity-25"
                          cx="12"
                          cy="12"
                          r="10"
                          stroke="currentColor"
                          strokeWidth="4"
                        />
                        <path
                          className="opacity-75"
                          fill="currentColor"
                          d="M4 12a8 8 0 018-8V0C5.373 0 0 5.373 0 12h4zm2 5.291A7.962 7.962 0 014 12H0c0 3.042 1.135 5.824 3 7.938l3-2.647z"
                        />
                      </svg>
                      Loading...
                    </>
                  )}
                </button>
              </div>
            </div>
          </div>
        </div>
      </Modal>
      {loading ? (
        <div className="h-full grid content-center text-center">
          <ReactLoading className="m-auto" type="bubbles" color="#626B79" height={65} width={65} />
        </div>
      ) : (
        <div>
          {product && product.data.length < 1 ? (
            <div className="mt-20">
              <div className="content-center text-center">
                <div className="mb-6">
                  <img src={rocket} alt="Rocket" className="object-cover m-auto" />
                </div>
                <div className="mb-4">
                  <h1 className="text-xl bold pb-1">No Products Here Yet</h1>
                  <p className="text-gray-400 text-md">
                    Add a new product to liven the place up a little
                  </p>
                </div>
                <button
                  type="button"
                  onClick={openModal}
                  className="px-6 py-2 mt-4 text-white bg-blue-600 rounded-lg text-sm hover:bg-blue-900"
                >
                  <FontAwesomeIcon
                    className="text-md mr-2 cursor-pointer"
                    style={{ color: 'white' }}
                    icon={faPlus}
                  />
                  Add New Product
                </button>
              </div>
            </div>
          ) : (
            <div className="mt-6 mb-6 mx-8">
              <div>
                <div className="mt-5 flex gap-5">
                  {/* <button
                    type="button"
                    onClick={openModal}
                    className="text-center px-12 py-18 rounded-md shadow-md"
                    style={{ backgroundColor: '#4B61DD' }}
                  >
                    <div className="pb-3">
                      <p className="text-sm text-white">New Product</p>
                    </div>
                    <FontAwesomeIcon className="text-4xl text-white" icon={faCirclePlus} />
                  </button> */}
                  {product.data.map((val, i) => (
                    <div className="rounded-md border border-gray-400 shadow-md">
                      <div className="relative overflow-hidden bg-white">
                        <img src="https://dummyimage.com/300" alt="Rocket" className="object-cover w-44 h-36 m-auto" />
                      </div>
                      <div className="p-3">
                        <div className="">
                          <div className="flex justify-between items-center">
                            <p className="text-sm">{val.name}</p>
                            <button onClick={() => addToCart(val.id)} type="button">+ cart</button>
                          </div>
                          <div>
                            <p className="text-sm pt-2">
                              Rp.
                              {val.price}
                            </p>
                          </div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          )}
        </div>
      )}
    </DashboardLayout>
  );
}
