import React from 'react';
import { Switch, Route, useRouteMatch } from 'react-router-dom';
import Test from './components/Test';

export default function index() {
  const { path } = useRouteMatch();
  return (
    <React.Fragment>
      <Switch>
        <Route exact path={path}>
          <Test />
        </Route>
      </Switch>
    </React.Fragment>
  );
}
