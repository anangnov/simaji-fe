import '../css/custom.css';

export default function Test() {
  return (
    <div className="content-center mt-8 mx-8 ">
      <button type="submit" className="nadi-primary-sur">
        test
      </button>
      <button type="submit" className="bg-indigo-600">
        test
      </button>
      <button type="submit" className="bg-blue-300">
        test
      </button>
      <button type="submit" className="bg-red-300">
        test
      </button>
      <button type="submit" className="bg-black2">
        test
      </button>
      {/* Daisy UI Example */}
      <div className="alert alert-success">
        <div className="flex-1">
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 24 24"
            className="w-6 h-6 mx-2 stroke-current"
          >
            <path
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M3 7v10a2 2 0 002 2h14a2 2 0 002-2V9a2 2 0 00-2-2h-6l-2-2H5a2 2 0 00-2 2z"
            />
          </svg>
          <label>Lorem ipsum dolor sit amet, consectetur adip!</label>
        </div>
      </div>
      {/* Daisy UI Example */}
    </div>
  );
}
