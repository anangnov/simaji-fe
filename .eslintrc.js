module.exports = {
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: ['eslint:recommended', 'plugin:react/recommended', 'airbnb'],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 13,
    sourceType: 'module',
  },
  plugins: ['react'],
  rules: {
    // add rules here
    'jsx-a11y/label-has-associated-control': 'off',
    'jsx-a11y/label-has-for': 'off',
    'react/jsx-no-useless-fragment': 'off',
    'react/jsx-fragments': 'off',
    'react/react-in-jsx-scope': 'off',
    'object-curly-newline': 'off',
    'import/prefer-default-export': 'off',
    'global-require': 'off',
    'no-dupe-keys': 'off',
    'import/no-unresolved': 'off',
    'comma-dangle': 'off',
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'no-confusing-arrow': ['error', { allowParens: true }],
  },
};
